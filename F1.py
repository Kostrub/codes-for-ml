# this file is a handwritten function for determining accuracy, recall, precision, and the F1 scores between two sets of data(labels - actual confirmed values) and guesses(some values predicted but not verified)
# the function shows how different methods of metrics portray effectiveness of the predictions over the labeled data.

label = [1, 0, 0, 1, 1, 1, 0, 1, 1, 1]
guess = [0, 1, 1, 1, 1, 0, 1, 0, 1, 0]

def f_1(labels, guesses):

    true_positives = 0
    true_negatives = 0
    false_positives = 0
    false_negatives = 0

    for i in range(len(guesses)):
      #True Positives
      if labels[i] == 1 and guesses[i] == 1:
        true_positives += 1
      #True Negatives
      if labels[i] == 0 and guesses[i] == 0:
        true_negatives += 1
      #False Positives
      if labels[i] == 0 and guesses[i] == 1:
        false_positives += 1
      #False Negatives
      if labels[i] == 1 and guesses[i] == 0:
        false_negatives += 1

    accuracy = (true_positives + true_negatives) / len(guesses) # len(guesses) is the same as true_positives+true_negatives+false_positives+false_negatives.
    print("The accuracy score is: {}".format(accuracy))

    recall = (true_positives / (true_positives + false_negatives))
    print("The recall score is: {}".format(recall))

    precision = true_positives / (true_positives + false_positives)
    print("The precision score is: {}".format(precision))

    f_1 = 2 * ((precision * recall) / (precision + recall)) # the harmonic mean of precision and recall.
    print("The F1 score is: {}".format(f_1))
