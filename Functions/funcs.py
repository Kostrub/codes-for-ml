import timeit
start = timeit.default_timer()
"""Assign functions to variables:"""

# def greet(name):
#     return "Hello " + name

# greet_someone = greet
# print(greet("Alex"))

"""Define functions inside functions:"""

# def greet(name):
#     def get_message():
#         return "Hello "

#     result = get_message() + name
#     return result

# print(greet("Alex"))

"""Functions passed as parameters to other Functions:"""

# def greet(name):
#     return "Hello " + name

# def call_func(func):
#     other_name = "Alex"
#     return func(other_name)

# print(call_func(greet))


"""Functions can return other Functions:"""

# def compose_greet_func():
#     def get_message():
#         return "Hello there!"

#     return get_message

# greet = compose_greet_func()

"""Inner functions have access to enclosing scope:"""

# def compose_greet_func(name):
#     def get_message():
#         return "Hello there "+ name + "!"

#     return get_message

# greet = compose_greet_func("Alex")
# print(greet())


"""Composition of Decorators:"""

# def get_text(name):
#     return "lorem ipsum, {0} dolor sit amet".format(name)

# def p_decorate(func):
#     def func_wrapper(name):
#         return "<p>{0}</p>".format(func(name))
#     return func_wrapper

# my_get_text = p_decorate(get_text)

# print(my_get_text("Alex"))

"""Python Decorator Syntax:"""

# def p_decorate(func):
#     def func_wrapper(name):
#         return "<p>{0}</p>".format(func(name))
#     return func_wrapper

# def strong_decorate(func):
#     def func_wrapper(name):
#         return "<strong>{0}</strong>".format(func(name))
#     return func_wrapper

# def div_decorate(func):
#     def func_wrapper(name):
#         return "<div>{0}</div>".format(func(name))
#     return func_wrapper


# @div_decorate
# @p_decorate
# @strong_decorate
# def get_text(name):
#     return "lorem ipsum, {0} dolor sit amet".format(name)

# print(get_text("Alex"))

"""Decorating Methods 1:"""


# def p_decorate(func):
#     def func_wrapper(self):
#         return "<p>{0}</p>".format(func(self))
#     return func_wrapper

# class Person(object):
#     def __init__(self):
#         self.name = "Alex"
#         self.family = "Kostrub"

#     @p_decorate
#     def get_fullname(self):
#         return self.name + " " + self.family

# my_person = Person()
# print(my_person.get_fullname())


"""Decorating Methods 2:"""


def p_decorate(func):
    def func_wrapper(*args, **kwargs):
        return "<p>{0}</p>".format(func(*args, **kwargs))
    return func_wrapper

class Person(object):
    def __init__(self):
        self.name = "Alex"
        self.family = "Kostrub"

    @p_decorate
    def get_fullname(self):
        return self.name + " " + self.family

my_person = Person()
print(my_person.get_fullname())

end = timeit.default_timer()

print(end-start)