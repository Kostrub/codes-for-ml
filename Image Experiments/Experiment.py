import os
from PIL import Image, ImageFilter
import timeit

start = timeit.default_timer()


# Open Image

image1 = Image.open("Sova.jpg")

#Rotate Image

# image1.rotate(90).save("Sova_rot90.jpg")


# Convert Image to B/W

# image1.convert(mode="L").save("Sova_BW.jpg")


# Blur image using Gaussian

# image1.filter(ImageFilter.GaussianBlur(15)).save("Sova_Blur.jpg")


image1_pixels = image1.load()
width, height = image1.width, image1.height
print(height)





end = timeit.default_timer()

print(end-start)