# A simple implementation of bubble-sort algorithm.

import random
ranum = [random.randint(0,100) for x in range(10)]

print("PRE SORT: {0}".format(ranum))

# Swap function that swaps values of neigbouring indices.
def swap(arr, index_1, index_2):
  # temp allows to avoid overwritting index 1 to index 2 before swapping the value of index 2 to index 1.
  temp = arr[index_1]
  arr[index_1] = arr[index_2]
  arr[index_2] = temp

# bubble-sort function with swap().
def bubble_sort_unoptimized(arr):

  # count of iterations for comparisson between optimized and unoptimized versions.
  iteration_count = 0
  # first loop visits every element in the aray.
  for el in arr:
    # second loop addresses each index in the array.
    for index in range(len(arr) - 1):
      iteration_count += 1
      # comparisson between array neigbouring indices.
      if arr[index] > arr[index + 1]:
        # if condiiton is met, swap() is called to swap the values.
        swap(arr, index, index + 1)

  print("PRE-OPTIMIZED ITERATION COUNT: {0}".format(iteration_count))

def bubble_sort(arr):
  iteration_count = 0
  #first loop visits every element in the aray.
  for i in range(len(arr)):
    # second loop addresses each index in the array THAT WAS NOT YET CHECKED AND SWAPPED/NOT SWAPPED.
    for idx in range(len(arr) - i - 1):
      iteration_count += 1
      if arr[idx] > arr[idx + 1]:
        # python is good because it allows simultaneous value swap without a function.
        arr[idx], arr[idx + 1] = arr[idx + 1], arr[idx]

  print("POST-OPTIMIZED ITERATION COUNT: {0}".format(iteration_count))

bubble_sort_unoptimized(ranum.copy())
bubble_sort(ranum)
print("POST SORT: {0}".format(ranum))
