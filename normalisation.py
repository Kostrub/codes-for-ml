# this file is made to show how data is normalised via min-max normalisation and Z-score normalisation.

import numpy as np
import matplotlib.pyplot as plt
from F1 import f_1

dates = [1897, 1998, 2000, 1948, 1962, 1950, 1975, 1960, 2017, 1937, 1968, 1996, 1944, 1891, 1995, 1948, 2011, 1965, 1891, 1978]

def min_max_normalization(data):
  mi = min(data)
  ma = max(data)
  new = []

  for i in  range(len(data)):
    # ("each data point" - "minimum value of the whole list")  / ("maximum value in the list" - "minimum value in the list") - min-max normalisation algorithmself.
    # min-max normalisation returns all values of the list in range 0-1.
    normalised = (data[i] - mi) / (ma - mi)
    new.append(normalised)
  new = sorted(new)
  return new

#print(min_max_normalization(dates))

def Z_score(data):
  sig = np.std(data)
  mu = np.mean(data)
  new = []
  for x in range(len(data)):
    # ("each data point in the list" - "mu aka mean value of the list") / "sigma aka the standard deviation of the list" - Z-score algorithm.
    # Z-score is good for dealing with outliers. It return negative and positive values.
    normalised = (data[x] - mu) / sig
    new.append(normalised)
  new = sorted(new)
  return new

#print(Z_score(dates))

# eperiment to explore Z_score normalised data normalised via min-max normalisation algorithm.
def min_max_Z(data):
  z_normalisation = Z_score(data)
  min_max = min_max_normalization(z_normalisation)
  return min_max


#print(min_max_Z(dates))

#print(f_1(min_max_Z(dates), min_max_normalization(dates)))


plt.plot(Z_score(dates),color='r')
plt.plot(min_max_normalization(dates),color='g')
plt.plot(min_max_Z(dates),color='b')
plt.show()
